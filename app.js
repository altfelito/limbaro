/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path')
  , gameContent = require('./synoAnto.js')
  , app = express()
  , session = require('express-session')
  , favicon = require('serve-favicon')
  , logger = require('morgan')
  , bodyParser = require('body-parser')
  , methodOverride = require('method-override')
  , cookieParser = require('cookie-parser')
  , errorHandler = require('errorhandler');

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
// app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser());
app.use(methodOverride());
app.use(cookieParser('your secret here'));
app.use(session());
// app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

// Setup routes
app.get('/game/start/', function (req, res) {
    if (!gameContent.ready) {
        return res.json(false);
    } else {
        start = true;
        return res.json(true);
    }
});

app.get('/game/stop/', function (req, res) {
    start = false;
    return res.json(true);
});

app.get('/get/question/', function (req, res) {
    var round = getRound();

    return res.json({
      id: round.id,
      question: round.question,
      answerA: round.answerA,
      answerB: round.answerB,
      answerC: round.answerC,
      answerD: round.answerD
    });
});
app.get('/get/answer/:id/:answer', function (req, res) {
    var id = req.params.id;
    var answer = req.params.answer;
    var round = getRound(id);
    console.log('Round: ' + JSON.stringify(round));
    console.log('Round answer: ' + round.correct);

    if (answer == round.correct) {
        return res.json(true);
    } else {
        return res.json(false);
    }
});
app.get('/', routes.index);

app.get('/dashboard', routes.dashboard);

// socket.io initialization
var server = http.createServer(app).listen(process.env.PORT || 3000);
var io = require('socket.io').listen(server);
io.set('log level', 1); // reduce logging

// Helper functions
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

getRound = function(id) {
    if (id) {
      return gameContent.getRound(id);
    }
    return gameContent.getRandomRound();
};



// Game logic
var players = {},
    start = false,
    gameDurationInSeconds = 30,
    timeLeft = {},
    unmatchedPlayers = [],
    timeoutTimer = {},
    matchedPlayers = {},
    playerSockets = {};

showTimer = function(playerId1, playerId2) {
  playerSockets[playerId1].emit('timer', timeLeft[playerId1 + playerId2]);
  playerSockets[playerId2].emit('timer', timeLeft[playerId1 + playerId2]);
  // io.sockets.emit('timer', {"players": [playerId1, playerId2], "timeLeft": timeLeft[playerId1 + playerId2]});
  if (timeLeft[playerId1 + playerId2] === 0) {
    clearInterval(timeoutTimer[playerId1 + playerId2]);
    playerSockets[playerId1].emit('gameOver', [players[playerId1], players[playerId2]]);
    playerSockets[playerId2].emit('gameOver', [players[playerId1], players[playerId2]]);
    // io.sockets.emit('gameOver', [players[playerId1], players[playerId2]]);
  }
  timeLeft[playerId1 + playerId2]--
  timeLeft[playerId2 + playerId1]--;
}

io.sockets.on('connection', function (socket) {
    socket.on('addPlayer', function(player) {
      playerSockets[socket.id] = socket;

      players[socket.id] = player;
      players[socket.id].id = socket.id;

      console.log("Player " + player.userName + " with id: " + socket.id + " has joined.");
      for (var key in players) {
          console.log("Players: " + key + " : " + players[key].userName);
      }

      socket.emit('playerId', socket.id);
      unmatchedPlayers.push(socket);

      if (unmatchedPlayers.length > 1) {
        socket.emit('ready', true);
        unmatchedPlayers[0].emit('ready', true);
        timeLeft[unmatchedPlayers[0].id + unmatchedPlayers[1].id] = gameDurationInSeconds;
        timeLeft[unmatchedPlayers[1].id + unmatchedPlayers[0].id] = gameDurationInSeconds;

        setImmediate(showTimer, unmatchedPlayers[0].id, unmatchedPlayers[1].id);
        timeoutTimer[unmatchedPlayers[0].id + unmatchedPlayers[1].id] =
                  setInterval(showTimer, 1000, unmatchedPlayers[0].id, unmatchedPlayers[1].id);

        matchedPlayers[unmatchedPlayers[0].id] = unmatchedPlayers[1].id;
        matchedPlayers[unmatchedPlayers[1].id] = unmatchedPlayers[0].id;

        unmatchedPlayers.splice(0, 2);
      }
    });

    socket.on('logPoints', function(player) {
      if (players[socket.id] != null) {
          players[socket.id].score = player.score;
          console.log(players[socket.id].userName + ' score: ' + players[socket.id].score);
      }
    });

    socket.on('answerQuestion', function (playerId) {
        var matchedSocket = playerSockets[matchedPlayers[playerId]];
        var gameContentRound = getRound();
        gameContentRound.answers++;
        if (timeLeft[players[playerId].id + matchedPlayers[playerId]] > 0) {
          socket.emit('nextRound', playerId);
          socket.emit('updateProgressBar', [players[playerId], players[matchedPlayers[playerId]]]);
          matchedSocket.emit('updateProgressBar', [players[playerId], players[matchedPlayers[playerId]]]);
        }
    });

    socket.on('disconnect', function() {
        if (players[socket.id] != null) {
          console.log("Player " + players[socket.id].userName + " with id: " + socket.id + " has left.");

          for (var key in players) {
              console.log("Remaining players: " + key + " : " + players[key].userName);
          }
        }
    });
});
