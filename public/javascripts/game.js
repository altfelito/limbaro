$(function() {
    var socket = io.connect();

    var Game = {
        players: [],
        ready: false,
        round: 1,
        currentQuestionid: 0,
        start: function () {
            Game.ready = true;
            $("#waiting").hide();
            $(".intro").hide();
            $(".player-info").hide();
            $(".page-header").hide();
            $(".results").hide();
            var startGame = $.get('/game/start/');
            startGame.done(function (data) {
               if (!data) {
               } else {
                   Game.startRound();
               }
            });
        },
        stop: function () {
            Game.ready = false;
            Game.round = 1;
            Player.score = 0;
            $("#waiting").show();
            $(".player-info").show();
            $(".game").hide();
            $(".intro").hide();
            $.get('/game/stop/');
        },
        startRound: function() {
            Game.ready = true;
            Game.enableAnswerField();
            $("#starting").hide();
            $.get('/get/question/', function (question) {
                Game.setQuestion(question);
            });
            $(".game").show();
        },
        setQuestion: function(question) {
            console.log("this is the question: " + question.question);
            // $('#year').val('');
            Game.currentQuestionid = question.id;
            $('.question').text(question.question);
            $('.round-count').text('Round ' + Game.round)
            $('.game').show();
            $('#answerABtn').html(question.answerA);
            $('#answerBBtn').html(question.answerB);
            $('#answerCBtn').html(question.answerC);
            $('#answerDBtn').html(question.answerD);
        },
        answerQuestion: function(answer, questionId) {
            $.get('/get/answer/' + questionId + '/' + answer, function (correct) {
                console.log("answer is: + " + answer);
                if (correct) {
                    Player.addPoints();
                    Game.setButtonCorrect(answer);
                } else {
                    Player.deductPoints();
                    Game.setButtonIncorrect(answer);
                }

                console.log("Player (" + Player.userName + ") score: " + Player.score);
                $(".score").text(Player.score);

                Game.disableAnswerField();
                setTimeout(function(){
                  socket.emit("answerQuestion", Player.id);
                }, 250);
            });
        },
        setButtonCorrect: function(answer) {
            switch (answer) {
              case "A":
                $("#answerABtn").removeClass("btn-outline-primary");
                $("#answerABtn").addClass("btn-success");
                break;
              case "B":
                $("#answerBBtn").removeClass("btn-outline-primary");
                $("#answerBBtn").addClass("btn-success");
                break;
              case "C":
                $("#answerCBtn").removeClass("btn-outline-primary");
                $("#answerCBtn").addClass("btn-success");
                break;
              case "D":
                $("#answerDBtn").removeClass("btn-outline-primary");
                $("#answerDBtn").addClass("btn-success");
                break;
            }
        },
        setButtonIncorrect: function(answer) {
            switch (answer) {
              case "A":
                $("#answerABtn").removeClass("btn-outline-primary");
                $("#answerABtn").addClass("btn-danger");
                break;
              case "B":
                $("#answerBBtn").removeClass("btn-outline-primary");
                $("#answerBBtn").addClass("btn-danger");
                break;
              case "C":
                $("#answerCBtn").removeClass("btn-outline-primary");
                $("#answerCBtn").addClass("btn-danger");
                break;
              case "D":
                $("#answerDBtn").removeClass("btn-outline-primary");
                $("#answerDBtn").addClass("btn-danger");
                break;
            }
        },
        showResults: function(players) {
          if (players[0].id === Player.id || players[1].id === Player.id) {
            $("#waiting").hide();
            $(".player-info").hide();
            $(".game").hide();
            $(".intro").hide();
            var me;
            var opponent;
            if (players[0].id === Player.id) {
              me = players[0];
              opponent = players[1];
            }
            else {
              me = players[1];
              opponent = players[0];
            }

            if (me.score > opponent.score) {
                // Congrats! You won!
                $('.results-success').show();
            }
            if (me.score < opponent.score) {
                // Haha! You lost!
                $('.results-fail').show();
            }
            if (me.score == opponent.score) {
                // Lame, you drew
                $('.results-draw').show();
            }

            // $('.stats-me-id').text(me.id);
            $('.stats-me-username').text(me.userName);
            $('.stats-me-score').text(me.score);
            // $('.stats-opponent-id').text(opponent.id);
            $('.stats-opponent-username').text(opponent.userName);
            $('.stats-opponent-score').text(opponent.score);
            $('.results').show();
          }
        },
        updateProgressBar: function(players) {
          var me;
          var opponent;
          if (players[0].id === Player.id) {
            me = players[0];
            opponent = players[1];
          }
          else {
            me = players[1];
            opponent = players[0];
          }

          var mywidth = (me.score + 1) / (opponent.score + 1 + me.score + 1) * 100;
          var opponentwidth = 100 - mywidth;
          $('#myscore').width(mywidth + "%");
          $('#opponentscore').width(opponentwidth + "%");
        },
        enableAnswerField: function() {
            $("#answerABtn").prop('disabled', false);
            $("#answerBBtn").prop('disabled', false);
            $("#answerCBtn").prop('disabled', false);
            $("#answerDBtn").prop('disabled', false);
        },
        disableAnswerField: function() {
            $("#answerABtn").prop('disabled', true);
            $("#answerBBtn").prop('disabled', true);
            $("#answerCBtn").prop('disabled', true);
            $("#answerDBtn").prop('disabled', true);
        }
    };

    var Player = {
        id: '',
        userName: '',
        score: 0,
        join: function () {
            Game.players.push(this);
            socket.emit("addPlayer", this);
        },
        addPoints: function() {
            Player.score = Player.score + 1;
            socket.emit("logPoints", this);
        },
        deductPoints: function() {
            socket.emit("logPoints", this);
        }
    };

    $("#play").click(function (e) {
        e.preventDefault();
        // $("#player").formError( {remove:true});
        if ($("#player").val().length === 0) {
          // $("#player").formError("Name cannot be empty");
          alert("name is empty!");
          console.log("name is empty!");
        }
        else {
          $("#waiting").show();
          Player.userName = $("#player").val();
          setTimeout(function(){
            Player.join();
          }, 1000);
          $("#player").attr('disabled', 'disabled');
          $("#join").addClass('disabled');
        }
        //$("#playerInfo").hide();
        //$("#welcome").html("Hello, " + Player.userName + "!");
    });

    $("#answerABtn").click(function (e) {
        $(this).blur();
        e.preventDefault();
        Game.answerQuestion("A", Game.currentQuestionid);
    });

    $("#answerBBtn").click(function (e) {
        $(this).blur();
        e.preventDefault();
        Game.answerQuestion("B", Game.currentQuestionid);
    });

    $("#answerCBtn").click(function (e) {
        $(this).blur();
        e.preventDefault();
        Game.answerQuestion("C", Game.currentQuestionid);
    });

    $("#answerDBtn").click(function (e) {
        $(this).blur();
        e.preventDefault();
        Game.answerQuestion("D", Game.currentQuestionid);
    });

    // $("#get-started").click(function (e) {
    //     e.preventDefault();
    //     $(".intro").hide();
    //     $(".player-info").show();
    // });

    $("#retry").click(function (e) {

    });

    socket.on('playerId', function(id) {
        Player.id = id;
        console.log(Player.userName + ' with id ' + Player.id);
    });

    socket.on('nextRound', function (playerId) {
        console.log('Moving onto the next round...');
        // if (playerId == Player.id) {
          Game.round++;
          Game.startRound();
        // }
        $("#answerABtn").removeClass("btn-success");
        $("#answerBBtn").removeClass("btn-success");
        $("#answerCBtn").removeClass("btn-success");
        $("#answerDBtn").removeClass("btn-success");

        $("#answerABtn").removeClass("btn-danger");
        $("#answerBBtn").removeClass("btn-danger");
        $("#answerCBtn").removeClass("btn-danger");
        $("#answerDBtn").removeClass("btn-danger");

        $("#answerABtn").addClass("btn-outline-primary");
        $("#answerBBtn").addClass("btn-outline-primary");
        $("#answerCBtn").addClass("btn-outline-primary");
        $("#answerDBtn").addClass("btn-outline-primary");

        // $("#answerABtn").addClass("mobileHoverFix");
        // $("#answerABtn").addClass("mobileHoverFix");
        // $("#answerABtn").addClass("mobileHoverFix");
        // $("#answerABtn").addClass("mobileHoverFix");
        $("#answerABtn").removeClass('ui-btn-active ui-focus');
        $("#answerBBtn").removeClass('ui-btn-active ui-focus');
        $("#answerCBtn").removeClass('ui-btn-active ui-focus');
        $("#answerDBtn").removeClass('ui-btn-active ui-focus');
    });

    socket.on('gameOver', function (players) {
      console.log('Game over! Well played!');
      Game.stop();
      Game.showResults(players);
    });

    socket.on('updateProgressBar', function(players) {
      Game.updateProgressBar(players);
    });

    socket.on('playerLeft', function(player) {
        Game.players.splice(player, 1);
    });

    socket.on('ready', function(ready) {
      console.log('this is ready: ' + ready);
        if (ready) {
            console.log('Game has 2 players, ready to start!');
            Game.start();
        } else {
            console.log('1 or more players have left, resetting game...');
            Game.stop();
        }
    });

    var initialOffset = 440;
    var i = 1;
    var time = 30;
    $('.circle_animation').css('stroke-dashoffset', initialOffset-(30*(initialOffset/time)));
    socket.on('timer', function(timeLeft) {
        // $('.timer').text(duration);
        // $('.circle_animation').css('stroke-dashoffset', initialOffset-((i+1)*(initialOffset/time)));
        // i++;
          // i = timeLeft;
          $('#timer').text(timeLeft);
      		if (timeLeft == 0) {
            // clearInterval(interval);
      			return;
          }
          $('.circle_animation').css('stroke-dashoffset', initialOffset-(timeLeft*(initialOffset/time)));
          // i++;

    });
});

function hoverTouchUnstick() {
  // Check if the device supports touch events
  if('ontouchstart' in document.documentElement) {
    // Loop through each stylesheet
    for(var sheetI = document.styleSheets.length - 1; sheetI >= 0; sheetI--) {
      var sheet = document.styleSheets[sheetI];
      // Verify if cssRules exists in sheet
      if(sheet.cssRules) {
        // Loop through each rule in sheet
        for(var ruleI = sheet.cssRules.length - 1; ruleI >= 0; ruleI--) {
          var rule = sheet.cssRules[ruleI];
          // Verify rule has selector text
          if(rule.selectorText) {
            // Replace hover psuedo-class with active psuedo-class
            rule.selectorText = rule.selectorText.replace(":hover", ":active");
          }
        }
      }
    }
  }
}

hoverTouchUnstick();
