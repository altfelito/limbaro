var questions = [{
 id: 1,
 question: "Find the odd one out",
 A: "good",
 B: "great",
 C: "excellent",
 D: "unpleasant",
 correct: "D"
},
{
 id: 2,
 question: "Find the odd one out.",
 A: "bad",
 B: "great",
 C: "excellent",
 D: "fantastic",
 correct: "A"
},
{
 id: 3,
 question: "Find the odd one out.",
 A: "interesting",
 B: "fascinating",
 C: "boring",
 D: "intriguing",
 correct: "C"
},
{
 id: 4,
 question: "Find the odd one out.",
 A: "cherry",
 B: "apple",
 C: "horse",
 D: "pear",
 correct: "C"
},
{
 id: 5,
 question: "Find the odd one out.",
 A: "store",
 B: "shop",
 C: "mall",
 D: "savings",
 correct: "D"
},
{
 id: 6,
 question: "Find the odd one out.",
 A: "cat",
 B: "hamster",
 C: "pet",
 D: "dinosaur",
 correct: "D"
},
{
 id: 7,
 question: "Find the odd one out.",
 A: "careful",
 B: "careless",
 C: "cautious",
 D: "accurate",
 correct: "B"
},
{
 id: 8,
 question: "Find the odd one out.",
 A: "drive",
 B: "car",
 C: "wheel",
 D: "pepper",
 correct: "D"
},
{
 id: 9,
 question: "Find the odd one out.",
 A: "computer",
 B: "mouse",
 C: "click",
 D: "perfume",
 correct: "D"
},
{
 id: 10,
 question: "Find the odd one out.",
 A: "chick",
 B: "cheek",
 C: "hen",
 D: "egg",
 correct: "B"
},
{
 id: 11,
 question: "Find the odd one out.",
 A: "movie",
 B: "cinema",
 C: "film",
 D: "filter",
 correct: "D"
},
{
 id: 12,
 question: "Find the odd one out.",
 A: "cry",
 B: "weep",
 C: "violet",
 D: "howl",
 correct: "C"
},
{
 id: 13,
 question: "Find the odd one out.",
 A: "mouse",
 B: "mouth",
 C: "mice",
 D: "rat",
 correct: "B"
},
{
 id: 14,
 question: "Find the odd one out.",
 A: "pepper",
 B: "onion",
 C: "peach",
 D: "cucumber",
 correct: "C"
},
{
 id: 15,
 question: "Find the odd one out.",
 A: "cup",
 B: "glass",
 C: "mug",
 D: "door",
 correct: "D"
},
{
 id: 16,
 question: "Find the odd one out.",
 A: "wine",
 B: "whiskey",
 C: "beer",
 D: "bear",
 correct: "D"
},
{
 id: 17,
 question: "Find the odd one out",
 A: "brush",
 B: "shampoo",
 C: "conditioner",
 D: "fridge",
 correct: "D"
},
{
 id: 18,
 question: "Find the odd one out.",
 A: "storm",
 B: "cow",
 C: "wind",
 D: "rain",
 correct: "B"
},
{
 id: 19,
 question: "Find the odd one out.",
 A: "donkey",
 B: "horse",
 C: "pony",
 D: "whale",
 correct: "D"
},
{
 id: 20,
 question: "Find the odd one out",
 A: "door",
 B: "floor",
 C: "wall",
 D: "call",
 correct: "D"
},
{
 id: 21,
 question: "Find the odd one out.",
 A: "cream",
 B: "mask",
 C: "patches",
 D: "tree",
 correct: "D"
},
{
 id: 22,
 question: "Find the odd one out",
 A: "fridge",
 B: "kettle",
 C: "dishwasher",
 D: "famous",
 correct: "D"
},
{
 id: 23,
 question: "Find the odd one out.",
 A: "salmon",
 B: "chicken",
 C: "cod",
 D: "herring",
 correct: "B"
},
{
 id: 24,
 question: "Find the odd one out.",
 A: "rose",
 B: "lily",
 C: "cinnamon",
 D: "daisy",
 correct: "C"
},
{
 id: 25,
 question: "Find the odd one out",
 A: "tea",
 B: "coffee",
 C: "juice",
 D: "finger",
 correct: "D"
},
{
 id: 26,
 question: "Find the odd one out.",
 A: "chicken",
 B: "cheese",
 C: "beef",
 D: "pork",
 correct: "B"
},
{
 id: 27,
 question: "Find the odd one out.",
 A: "spider",
 B: "sailor",
 C: "manager",
 D: "doctor",
 correct: "A"
},
{
 id: 28,
 question: "Find the odd one out",
 A: "leg",
 B: "arm",
 C: "head",
 D: "liver",
 correct: "D"
},
{
 id: 29,
 question: "Find the odd one out.",
 A: "cake",
 B: "pie",
 C: "lie",
 D: "jam",
 correct: "C"
},
{
 id: 30,
 question: "Find the odd one out.",
 A: "play",
 B: "gamble",
 C: "perform",
 D: "rest",
 correct: "D"
}]

rounds = [],
ready = true;

var getRandomRound = function() {
    var question = questions[Math.floor(Math.random() * questions.length)];

    var round = {
        id: question.id,
        question: question.question,
        correct: question.correct,
        answerA: question.A,
        answerB: question.B,
        answerC: question.C,
        answerD: question.D,
        answers: 0
    }

    return round;
}

var getRound = function(id) {
  for(var i in questions){
      if (questions[i].id == id) {
         return {
             question: questions[i].question,
             correct: questions[i].correct,
             answerA: questions[i].A,
             answerB: questions[i].B,
             answerC: questions[i].C,
             answerD: questions[i].D
         };
      }
  }
}

exports.ready = ready;
exports.getRandomRound = getRandomRound;
exports.getRound = getRound;
